﻿//Задание 3
// Ввести n чисел (n задается пользователем). Посчитать сумму трех первых нечетных.

int tmp = 0;
while (true)
{
    Console.WriteLine("Введите количество цифр");  
    
    try
    {
        int number = int.Parse(Console.ReadLine());
        int inp = 0; 
        int remains = 0;
        int check = 0; 

        for (int i = 0; i < number; i++)
        {

            Console.WriteLine("Вводите числа");
            if (int.TryParse(Console.ReadLine(), out var input))
            {

                int digits = 0;
                int check_input = input;
            

            int answer = 0;

            while (check_input > 0)
            {
                digits++;
                check_input /= 10;
            }

            if (digits > 3)
            {
                throw new ArgumentOutOfRangeException();
                
            }

                if (input % 2 != 0)
                {
                    check++;
                    inp = input;
                    tmp = inp;

                    if (check < 4)
                    {

                        for (int n = 0; n < 4; n++)
                        {
                            remains = input % 10;
                            input /= 10;
                            answer = answer + remains;
                        }
                        Console.WriteLine("Ответ = " + answer);
                    }
                }
            }
        }
    }
    catch (ArgumentOutOfRangeException)
    {
        Console.WriteLine("Вы ввели больше 3 знаков");
    }
    catch(FormatException)
    {
        Console.WriteLine("Ошибка. Должно быть введено целое значение");
    }
}
