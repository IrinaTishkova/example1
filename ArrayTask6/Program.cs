﻿//Считать из файла массив целых чисел. Упорядочить по убыванию.
//Вывести обратно в файл.

int[] array = File.ReadAllText(@"C:\Users\Ирина\source\repos\Sum\ArrayTask6\obj\file.txt").
            Split(new Char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries).

            Select(x => int.Parse(x)).ToArray();
for (int i = 0; i < array.Length; i++)
{
    Console.Write(array[i] + " ");
}
Console.WriteLine();

for (int i = 0; i < array.Length; i++)
{
    for (int j = 0; j < array.Length - 1; j++)
    {
        if (array[j] < array[j + 1])
        {
            int min = array[j + 1];
            array[j + 1] = array[j];
            array[j] = min;
        }

    }
}

Console.WriteLine("Отсортированный массив");
for (int i = 0; i < array.Length; i++)
{
    Console.Write(array[i] + " ");
}

File.AppendAllText(@"C:\Users\Ирина\source\repos\Sum\ArrayTask6\obj\out.txt", String.Join(", ", array));

Console.ReadLine(); ;