﻿// Вывод прямоугольника

while (true)
{
    try
    {
        int n, m;
        Console.Write("Введите m = ");
        m = Convert.ToInt32(Console.ReadLine());
        Console.Write("Введите n = ");
        n = Convert.ToInt32(Console.ReadLine());

        if (m > 20 || n > 20)
        {
            Console.WriteLine("Ошибка");
        }

        Console.WriteLine("Прямоугольник");

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
    }
    catch (Exception e)
    {
        Console.WriteLine("Ошибка");
    }
}