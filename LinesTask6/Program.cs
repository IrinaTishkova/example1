﻿//Вставить в строку другую строку
//- Какой ... день
//- замечательный

Console.WriteLine("Задание #6");
string str = "Какой день";
string modified = str.Insert((str.IndexOf(' ')), " замечательный");
Console.WriteLine(modified);

//Заменить в строке слово "магазин" на "парк" 
//- Привет, я иду в магазин

Console.WriteLine("\nЗадание #7");
string lines = "Привет, я иду в магазин";
Console.WriteLine(lines.Replace("магазин", "парк"));

//Удалить из строки слово "большого"
//- Сегодня в зоопарке я видел большого жирафа

Console.WriteLine("\nЗадание #8");
string strTask8 = "Сегодня в зоопарке я видел большого жирафа";
int index=strTask8.IndexOf(" большого");
int quantity = " большого".Length;
Console.WriteLine(strTask8.Remove(index, quantity));

//Привести предложение "ПрыгаЮщие БуквЫ" к нижнему, а затем к верхнему регистру
Console.WriteLine("\nЗадание #9");
string strTask9 = "ПрыгаЮщие БуквЫ";
Console.WriteLine(strTask9.ToUpper());
Console.WriteLine(strTask9.ToLower());

//Разделить строку на элементы массива
//- Первый рабочий день прошел на ура

Console.WriteLine("\nЗадание #10");
string strTask10 = "Первый рабочий день прошел на ура";
string[] array = strTask10.Split(' ');
for(int i=0; i<array.Length; i++)
{
    Console.WriteLine(array[i]);
}