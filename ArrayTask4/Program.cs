﻿//Ввести два упорядоченных массива (контроль за корректностью ввода). 
//Слить их в один упорядоченный массив без использования сортировки.


while(true)
{
    try
    {
        Console.Write("Введите размерность первого массива = ");

        int quantityFirst = int.Parse(Console.ReadLine());
        int[] arrayFirst = new int[quantityFirst];

        int indexFirst = 0;
        while (indexFirst < quantityFirst)
        {
            try
            {

                Console.Write("{0} элемент = ", indexFirst + 1);
                if (int.TryParse(Console.ReadLine(), out int tmp))
                {
                    if (indexFirst == 0)
                    {
                        arrayFirst[indexFirst] = tmp;
                        indexFirst++;
                        continue;
                    }
                    if (tmp < arrayFirst[indexFirst - 1])
                    {
                        throw new ArgumentOutOfRangeException("Error: is less. Try input again");
                        continue;
                    }
                    arrayFirst[indexFirst] = tmp;
                    indexFirst++;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Error: cannot parse. Try input again.");
                    continue;
                }

            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Ошибка. Каждое последующее число должно быть меньше предыдущего");
            }
        }

        Console.Write("Введие размерность второго массива = ");

        int quantitySecond = int.Parse(Console.ReadLine());
        int[] arraySecond = new int[quantitySecond];
        int change = 0;

        int indexSecond = 0;
        while(indexSecond < quantitySecond)
        {
            try
            {
                
                Console.Write("{0} элемент = ", indexSecond + 1);
                if (int.TryParse(Console.ReadLine(), out int tmp))
                {
                    if (indexSecond == 0)
                    {
                        arraySecond[indexSecond] = tmp;
                        indexSecond++;
                        continue;
                    }
                    if (tmp < arraySecond[indexSecond - 1])
                    {

                        throw new ArgumentOutOfRangeException("Error: is less. Try input again");
                        continue;
                    }
                    arraySecond[indexSecond] = tmp;
                    indexSecond++;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Error: cannot parse. Try input again.");
                    continue;
                }
                
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Ошибка. Каждое последующее число должно быть меньше предыдущего");
            }
        }

        int[] arraySplit = new int[quantityFirst + quantitySecond];

        int l = 0;
        int m = 0;
        int k = 0;
        while (l<quantityFirst && m<quantitySecond)
        {
            if(arrayFirst[l]<=arraySecond[m])
            {
                arraySplit[k] = arrayFirst[l];
                l++;
            }
            else if(arraySecond[m] < arrayFirst[l])
            {
                arraySplit[k] = arraySecond[m];
                m++;
            }
            k++;
        }

        if (l == quantityFirst)
        {
            Array.Copy(arraySecond, m, arraySplit, k, quantitySecond - m);
        }
        else if (m == quantitySecond)
        {
            Array.Copy(arrayFirst, l, arraySplit, k, quantityFirst - l);
        }

        for (int i = 0; i < arraySplit.Length; i++)
        {
            Console.WriteLine(arraySplit[i] + " ");
        }

    }
    catch(FormatException ex)
    {
        Console.WriteLine("Ошибка");
    }
}

