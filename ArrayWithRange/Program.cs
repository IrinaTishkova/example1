﻿//Ввести массив целых чисел в диапазоне [-100,100].
//Длина массива задается пользователем. 
//а) Найти минимальный элемент
//б) Найти максимальный элемент
//в) найти минимальное нечетное
//г) найти минимальное четное
//д) найти минимальный и максимальный элементы и поменять их местами

while (true)
{
    try
    {
        Console.Write("Введите длину массива = ");
        int quantity = int.Parse(Console.ReadLine());
        int[] array = new int[quantity];
           
        for (int i = 0; i < quantity; i++)   
        {    
            Console.Write("{0} элемент = ", i);
              
            if(int.TryParse(Console.ReadLine(), out int tmp) && tmp < 100 && tmp > -100)            
            {
                array[i] = tmp;
            }          
        }

        int min = array[0];
        int min_index = 0;

        for (int i = 0; i < quantity; i++)
        {
            if (min > array[i])
            {
                min = array[i];
                min_index = i;
            }
        }
       
        int max = array[0];
        int max_index = 0;
        for (int i = 0; i < quantity; i++)
        {
            if (max < array[i])
            {
                max = array[i];
                max_index = i;
            }
        }
       
        Console.WriteLine("Минимальный {0} элемент массива = {1}", min_index, min);
        Console.WriteLine("Максимальный {0} элемент массива = {1}", max_index, max);


        int odd = array[0];
        for (int i = 0; i < quantity; i++)
        {
            if(Math.Abs(array[i]%2)==1 && odd>array[i])
            {
                odd = array[i];
            }  
        }

        int even = array[0];
        for(int i=0; i<quantity; i++)
        {
            if((array[i]%2)==0 && even>array[i])
            {
                even = array[i];
            }
        }
  
        Console.WriteLine("Минимальное четное = " + even);
        Console.WriteLine("Минимальное нечетное = " + odd);
        
        for(int i=0; i<quantity; i++)
        {
            if(array[min_index]> array[i])
            {
                min = i;
            }
            if(array[max_index]<array[i])
            {
                max = i;
            }

        }
        int change = array[min_index];
        array[min_index] = array[max_index];
        array[max_index] = change;

        for(int i=0; i<quantity; i++)
        {
            Console.WriteLine("Массив, где максимальный и минимальный элементы поменяны:\n"+ array[i]);
        }


    }
    catch(ArgumentOutOfRangeException)
    {
        Console.WriteLine("Ошибка. Введенное значение должно быть в диапазоне [-100; 100]");
    }
    catch(FormatException)
    {
        Console.WriteLine("Ошибка. Должно быть введено целое число");
    }
    catch(IndexOutOfRangeException)
    {
        Console.WriteLine("Ошибка. Размер массива не может быть равен 0");
    }
}

