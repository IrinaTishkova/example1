﻿//Ввести n целых чисел (n задается пользователем).
//Какая цифра встречается чаще других?
//Если таких цифр несколько – вывести ту из них,
//которая обозначает наибольшее число, а также сколько раз она встретилась.

while (true)
{
    try
    {
        int quantity;
        Console.Write("Введите количество чисел в массиве: ");
        quantity = Convert.ToInt32(Console.ReadLine());
        int[] array;
        array = new int[quantity];

        for (int i = 0; i < quantity; i++)
        {
            Console.Write("{0} элемент = ", i);
            array[i] = Convert.ToInt32(Console.ReadLine());
        }

        int[] new_array;
        new_array = new int[quantity];
        int max = 0, a = 0, b = 0;
        int number = 0;

        for (int i = 0; i < quantity; i++)
        {
            for (int j = 0; j < quantity - 1; j++)
            {
                if (array[j] > array[j + 1])
                {
                    int t = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = t;
                }

            }
            Console.WriteLine("{0} элемент равен {1}", i, array[i]);
        }

        int num = array[0];
        int max_frg = 1;
        int[] check_array = new int[quantity];
        int schet = 0;

        for (int i = 0; i < quantity - 1; i++)
        {
            int frg = 1;
            for (int k = i + 1; k < quantity; k++)
            {
                if (array[i] == array[k])
                {
                    frg += 1;
                }
                if (frg > max_frg)
                {
                    max_frg = frg;
                    num = array[i];
                }
                if (frg == max_frg)
                {
                    check_array[i] = array[i];
                }
            }
        }

        for (int i = 0; i < quantity; i++)
        {
            for (int j = 0; j < quantity; j++)
            {
                if (check_array[i] > check_array[j])
                {
                    schet = check_array[i];
                }
            }
        }
        Console.WriteLine("Наибольшее число имеющее повторения = {0}", schet);
    } catch(FormatException)
    {
        Console.WriteLine("Ошибка. Должно быть введено целое число");
    }
    
}