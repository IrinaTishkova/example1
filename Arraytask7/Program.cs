﻿// Сдвинуть массив [1..n] циклически влево (вправо) на m позиций.
// «Падающие» элементы должны уходить в хвост (в голову).

while (true)
{
    try {
        Console.WriteLine("\nВведите размер массива");
        int quantity = int.Parse(Console.ReadLine());
        int[] array = new int[quantity];

        for (int i = 0; i < quantity; i++)
        {
            Console.Write("{0} элемент = ", i);
            if(int.TryParse(Console.ReadLine(), out int input))
            {
                array[i] = input;
            }
        }

        Console.WriteLine("На сколько элементов сдвинуть массив?");
        int change = Convert.ToInt32(Console.ReadLine());

        Console.Write("\nВыберите:\n1.Сдвиг вправо\n2.Сдвиг влево: ");
        int answer = Convert.ToInt32(Console.ReadLine());
        while(true)
        {
            switch (answer)
            {
                case 1:
                    Console.WriteLine("Сдвиг массива вправо");
                    for (int i = 0; i < change; ++i)
                    {
                        int aLast = array[quantity - 1];
                        for (int j = quantity - 1; j > 0; j--)
                        {
                            array[j] = array[j - 1];
                        }
                        array[0] = aLast;
                    }

                    for (int i = 0; i < quantity; i++)
                    {
                        Console.Write(array[i] + " ");
                    }
                    break;

                case 2:
                    for (int i = 0; i < change; ++i)
                    {
                        int tmp = array[0];
                        for (int j = 0; j < quantity - 1; j++)
                        {
                            array[j] = array[j + 1];
                        }
                        array[quantity - 1] = tmp;
                    }

                    Console.WriteLine("Сдвиг массива влево");

                    for (int i = 0; i < quantity; i++)
                    {
                        Console.Write(array[i] + " ");
                    }
                    break;

                default:
                    break;
            }
            
        }
    }
    catch(FormatException)
    {
        Console.WriteLine("Ошибка. Должно быть введено целое число");
    }
}



