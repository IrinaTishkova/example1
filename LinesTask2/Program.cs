﻿//Дан массив строк ["apple", "banana", "orange", "kiwi", "mango"]
//-Вывести все значения через ", "
//- Вывести все значения построчно

string[] array = { "apple", "banana", "orange", "kiwi", "mango" };

Console.Write(string.Join(", ", array));
Console.Write(string.Join("\n", array));