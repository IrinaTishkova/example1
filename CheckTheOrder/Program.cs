﻿//Ввести массив целых чисел. Длина массива задается пользователем. Определить,
//упорядочен ли он по возрастанию. По убыванию?

while (true)
{
    try
    {
        int quantity;
        Console.Write("Введите количество чисел в массиве: ");
        quantity = Convert.ToInt32(Console.ReadLine());
        int[] array;
        array = new int[quantity];

        for (int i = 0; i < quantity; i++)
        {
            Console.Write("{0} элемент = ", i);
            array[i] = Convert.ToInt32(Console.ReadLine());
        }

        bool max = false;

        for (int i = 0; i < quantity; i++)
        {
            for (int j = 0; j < quantity - 1; j++)
            {
                if (array[j] > array[j + 1])
                {
                    max = true;
                }

            }
        }

        if (max == true)
        {
            Console.WriteLine("Массив упорядочен по убыванию");
        }
        else Console.WriteLine("Массив упорядочен по возрастанию");

    }
    catch (FormatException)
    {
        Console.WriteLine("Ошибка. Введите целочисленное значение");
    }
}
