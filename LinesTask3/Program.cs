﻿//Сравнить две строки и вывести результат сравнения
//- "привет" и "здравствуйте"
//- "двацдать" и "двенадцать"
//- "синус" и "синусоида"
//- "14" и "81"

Console.WriteLine(String.Compare("привет", "здравствуйте"));
Console.WriteLine(String.Compare("двадцать", "двенадцать"));
Console.WriteLine(String.Compare("синус", "синусоида"));
Console.WriteLine(String.Compare("14", "81"));
