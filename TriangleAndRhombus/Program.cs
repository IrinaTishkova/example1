﻿// Вывод треугольника и ромба

while (true)
{
    try
    {
        int n;

        Console.Write("Введите n (не больше 20) = ");
        n = Convert.ToInt32(Console.ReadLine());

        if (n > 20)
        {
            throw new ArgumentOutOfRangeException();
            continue;
        }
        else
        {

            Console.WriteLine("Треугольник б");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n + 1; j++)
                {
                    if (i >= j)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine("Треугольник в");
            for (int i = 0; i < n+1; i++)
            {
                for (int j = n; j > 0; j--)
                {
                    if (i >= j)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine("Треугольник д");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i <= j)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine("Треугольник г");
            for (int i = 0; i < n; i++)
            {
                for (int j = n; j>0; j--)
                {
                    if (i < j)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine("Ромб");
            int z = 1;
            for (int i = 0; i <= n / 2; i++)
            {
                int prob = (n - z) / 2;

                for (int j = 0; j <= prob; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 0; k < z; k++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();

                z = z + 2;
            }

            z = n - 2;

            for (int i = n / 2; i > 0; i--)
            {
                int prob = (n - z) / 2;
                for (int j = 0; j <= prob; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 0; k < z; k++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
                z = z - 2;
            }
        }

    }
    catch (ArgumentOutOfRangeException)
    {
        Console.WriteLine("Ошибка. Значение должно быть меньше 20");
    }
    catch(FormatException)
    {
        Console.WriteLine("Значение должно быть целочисленным");
    }

}