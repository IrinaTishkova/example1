1. Какие вы знаете основные возможности CLR?
2. Какие применения метаданных вы можете назвать?
3. В чем отличие файлов с заголовками PE32 и PE32+?
4. Какие из возможностей CLR доступны при программировании на C#?
5. С помощью чего IL-код преобразовывается в машинные команды?
6. Перечислите некоторые возможности повышения производительности управляемого кода по сравнению с неуправляемым?
7. Что такое верификация?
8. Каким словом помечаются методы, содержащие небезопасный код?
9. В каких ситуациях полезна программа NGen.exe?
10. Какой существует способ отличать сборки разных программ?
11. Что такое маркер открытого ключа?
12. Для чего подписывают сборку?
