﻿//Ввести n чисел (n задается пользователем). Посчитать сумму трех последних нечетных.

while (true)
{
    int n1 = 0;
    int n2 = 0;
    int n3 = 0;
    int answer = 0;
    int answer2 = 0;
    int answer3 = 0;
    int remains = 0;
    int input = 0;
    string input_str;

    try
    {
        int number = Convert.ToInt32(Console.ReadLine());

        for (int i = 0; i < number; i++)
        {
            Console.WriteLine("Введите число:");
            input = Convert.ToInt32(Console.ReadLine());
            input_str = input.ToString();

            if (input % 2 != 0)
            {
                n1 = n2;
                n2 = n3;
                n3 = input;
            }

        }

        for (int n = 0; n < 4; n++)
        {
            remains = n1 % 10;
            n1 /= 10;
            answer = answer + remains;
        }

        for (int n = 0; n < 4; n++)
        {
            remains = n2 % 10;
            n2 /= 10;
            answer2 = answer2 + remains;
        }

        for (int n = 0; n < 4; n++)
        {
            remains = n3 % 10;
            n3 /= 10;
            answer3 = answer3 + remains;
        }

        Console.WriteLine("Сумма последних трех равна {0}, {1}, {2}", answer, answer2, answer3);

    }
    catch (ArgumentOutOfRangeException)
    {
        Console.WriteLine("Вы ввели больше 3 знаков");
    }
    catch (FormatException)
    {
        Console.WriteLine("Ошибка. Должно быть введено целое значение");
    }
}