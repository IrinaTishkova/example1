﻿//Даны переменные 
//hello = "Привет!"
//name = "Меня зовут ..."
//age = "Мне ... лет"
//Оперируя переменными составить полноценное предложение всеми возможными способами (форматирование, интерполяция)


Console.Write("Введите ваше имя ");
String nameUser = Console.ReadLine();
Console.Write("Введите ваш возраст ");
int ageUser = int.Parse(Console.ReadLine());
String hello = "Привет!";
String name = "Меня зовут";
String age = "Мне";
String ageYears=" лет";

Console.WriteLine("{0} {1} {2}. {3} {4} {5}", hello, name, nameUser, age, ageUser, ageYears);
Console.WriteLine(hello+ " "+ name + " " + nameUser+". " + age+" "+ ageUser+ " "+ageYears);
Console.WriteLine($"{hello} {name} {nameUser} {age} {ageUser} {ageYears}");
