﻿// Задание 1 и 2
// Ввести трехзначное число. Посчитать сумму цифр, вывести на экран.
// Вводить числа, пока не будет введен ноль. Вывести число с максимальной суммой цифр в нем

int answer_max = 0;
while (true)
{
    try
    {
        Console.WriteLine("Введите целое трехзначное число");
        int input = int.Parse(Console.ReadLine());
        int tmp = 0;
        int answer = 0;

        if (input == 0)
        {
            Console.WriteLine("Максимальная сумма равна " + answer_max);
            break;
        }

        int remains = 0;
        int dijits = 0;

        int new_input = input;

        while(new_input>0)
        {
            dijits++;
            new_input /= 10;
        }

        if (dijits > 3)
        {
            throw new ArgumentOutOfRangeException();
            
        }

        if(input<0)
        {
            throw new Exception();
        }

        for (int i = 0; i < 4; i++)    
        {     
            remains = input % 10;   
            input /= 10;     
            answer = answer + remains;  
        }
 
        Console.WriteLine("Ответ = " + answer);

        if (answer_max < answer)
        {    
            answer_max = answer; 
            tmp = answer_max;
        }
        
    }
    catch (ArgumentOutOfRangeException)
    {
        Console.WriteLine("Ошибка. Вы ввели больше 3 знаков");
    }
    catch (FormatException)
    {
        Console.WriteLine("Ошибка. Введеное значение должно быть целым трехзнычным числом");
    }
    catch(Exception)
    {
        Console.WriteLine("Ошибка. Число должно быть положительным");
    }

}
 