﻿
//Вывод прямоугольника
void Rectangle()
{ 
    while (true)
    {
        try
        {
            int n, m;
            Console.Write("Введите m = ");
            m = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите n = ");
            n = Convert.ToInt32(Console.ReadLine());

            if (m > 20 || n > 20)
            {
                Console.WriteLine("Ошибка");
            }

            Console.WriteLine("Прямоугольник");

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Ошибка");
        }
    }
}

//Вывод прямоугольного треугольника и ромба
void TriangleAndRhombus()
{
    while (true)
    {
        try
        {
            int n;

            Console.Write("Введите n (не больше 20) = ");
            n = Convert.ToInt32(Console.ReadLine());

            if (n > 20)
            {
                Console.WriteLine("Ошибка");
            }
            else
            {

                Console.WriteLine("Треугольник");
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n + 1; j++)
                    {
                        if (i >= j)
                        {
                            Console.Write("*");
                        }
                        else
                        {
                            Console.Write(" ");
                        }
                    }
                    Console.WriteLine();
                }


                Console.WriteLine("Ромб");
                int z = 1;
                for (int i = 0; i < n / 2 + 1; i++)
                {
                    int prob = (n - z) / 2;

                    for (int j = 0; j < prob; j++)
                    {
                        Console.Write(" ");
                    }
                    for (int k = 0; k < z; k++)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine();

                    z = z + 2;
                }

                z = n - 2;

                for (int i = n / 2; i > 0; i--)
                {
                    int prob = (n - z) / 2;
                    for (int j = 0; j < prob; j++)
                    {
                        Console.Write(" ");
                    }
                    for (int k = 0; k < z; k++)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine();
                    z = z - 2;
                }
            }

        }
        catch (Exception e)
        {
            Console.WriteLine("Ошибка");
        }
    }
}